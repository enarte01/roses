%Rosies Roses
%


roses(golden_sunset).
roses(cottageBeauty).
roses(mountainBloom).
roses(pinkParadise).
roses(sweetDreams).

event(annivary).
event(charity).
event(retirement).
event(prom).
event(wedding).


item(balloons).
item(candles).
item(chocolate).
item(cards).
item(stream).

customer(hugh).
customer(ida).
customer(leroy).
customer(jeremy).
customer(stella).





solve:-roses(HughRose),roses(IdaRose), roses(JeremyRose), roses(LeroyRose), roses(StellaRose),
all_different([HughRose, IdaRose, JeremyRose, LeroyRose, StellaRose ]),

event(HughEvent),event(IdaEvent),event(JeremyEvent),event(StellaEvent),event(LeroyEvent), 
all_different([HughEvent, IdaEvent, JeremyEvent, StellaEvent, LeroyEvent]),

item(HughItem),item(IdaItem),item(StellaItem),item(JeremyItem),item(LeroyItem),
all_different([HughItem, IdaItem, StellaItem, JeremyItem,LeroyItem]),



Triples = [ [hugh, HughRose, HughEvent, HughItem],
            [jeremy, JeremyRose, JeremyEvent, JeremyItem],
            [ida, IdaRose, IdaEvent, IdaItem],
            [stella, StellaRose,StellaItem, StellaEvent], 
            [leroy, LeroyRose,LeroyItem, LeroyEvent] ],


%\+ member([customer, rose, event, item], Triples),

%jeremy did not pick mountainBloom
\+ member([jeremy, mountainBloom, _, _ ], Triples),


%stella didnt didnt attend wedding 
\+ member([stella,_, wedding,_], Triples),



%Hugh did not attend a wedding
\+ member([hugh, _,wedding, _], Triples),

%Hugh did not attend auction
\+ member([hugh, _,charity, _], Triples),

%Hugh selected pinkParadise
member([hugh, pinkParadise,_,_], Triples),

%the customer who picked sweetDreams  also bought chocolates
member([_, sweetDreams, _,chocolates], Triples),

%the customer who who attended the prom also  bought candles
member([_,_ , prom,candles], Triples),

%jeremy  made a purchase for prom
member([jeremy, _,prom, _], Triples),

%stella chose cottageBeauty
member([stella, cottageBeauty,_,_], Triples),

% Leroy was shopping for retirement
member([leroy, _,retirement, _], Triples),



%the customer who picked roses for anniversary also bought streamers
member([_, _, anniversary,streamers], Triples),


%the customer who picked roses for wedding also bought balloons
member([_, _, wedding,balloons], Triples),

%the customer who picked sweetDreams  also bought chocolates
member([_, sweetDreams, _,chocolates], Triples),




tell(hugh, HughRose, HughEvent, HughItem),
tell(ida, IdaRose, IdaEvent, IdaItem),
tell(leroy, LeroyRose, LeroyEvent, LeroyItem ),
tell(jeremy, JeremyRose, jeremyEvent, jeremyItem ),
tell(stella, StellaRose, StellaEvent, StellaItem).

% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).


tell(X, Y,W, Z) :-
write('Customer'), write(X), write(' bought rose variety '), write(Y),
write('for event'), write(W), write('and also bought '), write(Z),  nl.

















